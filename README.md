# dimension-stickers
List of Stickers available for the Matrix Dimension Integration Manager

- [Untitled Goose Game Stickers](https://stickers.t2bot.io/pack/@benjamin:mstdn.social/IxWUiN6u4g-Untitled-Goose-Stic)
    - Taken from: https://signalstickers.com/pack/653e393a6b193c0b8287432a05ff3976

- [Little White Seal](https://stickers.t2bot.io/pack/@hbenjamin:kde.org/Enrmpk2YCn-Little-White-Seal)
    - Taken from: https://signalstickers.com/pack/0bf641e6bf5e281d8b17d63dd5b72005